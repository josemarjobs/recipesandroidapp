package com.kindelbit.recipiesapp;

/**
 * Created by josemarmagalhaes on 4/27/16.
 */
public class IngredientsFragment extends CheckBoxesFragment {
    @Override
    public String[] getContents(int index) {
        return Recipes.ingredients[index].split("`");
    }
}
