package com.kindelbit.recipiesapp;

/**
 * Created by josemarmagalhaes on 4/27/16.
 */
public class DirectionsFragment extends CheckBoxesFragment {
    @Override
    public String[] getContents(int index) {
        return Recipes.directions[index].split("`");
    }
}
