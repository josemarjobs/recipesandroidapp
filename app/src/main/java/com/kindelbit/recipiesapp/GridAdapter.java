package com.kindelbit.recipiesapp;

/**
 * Created by josemarmagalhaes on 4/27/16.
 */
public class GridAdapter extends RecyclerAdapter {
    private GridFragment.OnRecipeSelected listener;

    public GridAdapter(GridFragment.OnRecipeSelected listener) {
        this.listener = listener;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.grid_item;
    }

    @Override
    protected void onRecipeSelected(int index) {
        listener.onGridRecipeSelected(index);
    }
}
