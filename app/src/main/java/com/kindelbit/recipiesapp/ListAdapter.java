package com.kindelbit.recipiesapp;

/**
 * Created by josemarmagalhaes on 4/26/16.
 */
public class ListAdapter extends RecyclerAdapter {
    private ListFragment.OnRecipeSelected listener;

    public ListAdapter(ListFragment.OnRecipeSelected listener) {
        this.listener = listener;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.list_item;
    }

    @Override
    protected void onRecipeSelected(int index) {
        listener.onListRecipeSelected(index);
    }
}
